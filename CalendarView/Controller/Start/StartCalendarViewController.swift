//
//  StartCalendarViewController.swift
//  CalendarView
//
//  Created by Jakkawad Chaiplee on 5/9/2561 BE.
//  Copyright © 2561 Jakkawad Chaiplee. All rights reserved.
//

import UIKit

class StartCalendarViewController: UIViewController {

    var startDayOfMonth: Int = 0
    
    var developerMode: Bool = false
    
    var startDay: Int = 0
    var startMonth: Int = 0
    var startYear: Int = 0
    
    var dateCell = "dateCell"
    let dateCellHeight: CGFloat = 480.0
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        print("Start day: \(startDay),  month: \(startMonth), year: \(startYear), startDayOfMonth: \(startDayOfMonth)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StartCalendarViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dateCellHeight
    }
}

extension StartCalendarViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: dateCell) as? CalendarStartTableViewCell
        cell?.developerMode = developerMode
        cell?.startDayOfMonth = startDayOfMonth
        cell?.setDate(day: startDay, month: startMonth, year: startYear)
        cell?.selectionStyle = .none
        return cell!
    }
}
