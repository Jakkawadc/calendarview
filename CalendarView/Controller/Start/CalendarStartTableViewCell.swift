//
//  CalendarStartTableViewCell.swift
//  CalendarView
//
//  Created by Jakkawad Chaiplee on 5/9/2561 BE.
//  Copyright © 2561 Jakkawad Chaiplee. All rights reserved.
//

import UIKit

class CalendarStartTableViewCell: UITableViewCell {

    // Dev
    var startDayOfMonth: Int = 0
    var developerMode: Bool!
    //
    var currentDay: Int!
    var currentMonth: Int!
    var currentYear: Int!
    
    var thisMonth: Int!
    var thisYear: Int!
    
    var startDate: String!
    
    var monthArray = ["Empty", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    
    var nextMonthDisable: Bool = true
    
    let calendar = Calendar(identifier: .gregorian)
    
    @IBOutlet weak var dayNumberLabel: UILabel!
    @IBOutlet weak var monthNameLabel: UILabel!
    @IBOutlet weak var yearNumberLabel: UILabel!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var previousBtn: UIButton!
    
    @IBOutlet var dayBtns: [UIButton] = []
    
    @IBAction func nextBtnWasPressed(_ sender: Any) {
        
        if nextMonthDisable {
            
        } else {
            nextMonth()
        }
    }
    
    @IBAction func previousBtnWasPressed(_ sender: Any) {
        previousMonth()
    }
    func nextMonth() {
        if currentMonth > 11 {
            currentMonth = 1
            currentYear = currentYear + 1
        } else {
            currentMonth = currentMonth + 1
        }
        monthNameLabel.text = monthArray[currentMonth]
        
        setupCalendarUI()
        
        clearStartDateSelected()
    }
    func previousMonth() {
        if currentMonth <= 1 {
            currentMonth = 12
            currentYear = currentYear - 1
        } else {
            currentMonth = currentMonth - 1
        }
        monthNameLabel.text = monthArray[currentMonth]
        
        setupCalendarUI()
        
        clearStartDateSelected()
    }
    func clearStartDateSelected() {
        for item in dayBtns {
            item.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            item.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
        }
        startDate = ""
    }
    @objc func actionButtonWasPressed(sender:UIButton!) {
        for item in dayBtns {
            if item.tag == sender.tag {
                item.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                item.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.3647058824, blue: 0.8549019608, alpha: 1)
                item.layer.borderWidth = 1.0
                item.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                item.layer.cornerRadius = item.frame.size.width / 2
                item.clipsToBounds = true
                
                startDate = item.titleLabel?.text
                // NotiCenter => data
//                let passDate = (Int(startDate), currentMonth, currentYear)
//                notiCenter.post(name: .alertOverviewCellStartDate, object: passDate)
            } else {
                item.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                item.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            }
        }
    }
    func setDate(day: Int, month: Int, year: Int) {
        currentDay = day
        currentMonth = month
        currentYear = year
        // Dev
//        startDay = day
        //
        thisMonth = currentMonth
        thisYear = currentYear
        setupCalendarUI()
    }
    func setupCalendarUI() {
        // Get Date range
//        let calendar = Calendar.current
        let dateComponents = DateComponents(year: currentYear, month: currentMonth)
        let didDate = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: didDate)!
        let numDays = range.count
        var startDayOfMonth: Int = 0
        
        for item in 0..<numDays {
            
            let day = item + 1
            let workDay = getDayOfWeek("\(currentYear!)-\(currentMonth!)-\(day)")
            if day == 1 {
                startDayOfMonth = workDay!
            }
            
        }
        if numDays == 28 {
            setupMonthLayout(start: startDayOfMonth, numberOfDay: 28)
        } else if numDays == 29 {
            setupMonthLayout(start: startDayOfMonth, numberOfDay: 29)
        } else if numDays == 30 {
            setupMonthLayout(start: startDayOfMonth, numberOfDay: 30)
        } else if numDays == 31 {
            setupMonthLayout(start: startDayOfMonth, numberOfDay: 31)
        }
        
        dayNumberLabel.text = "\(currentDay!)"
        monthNameLabel.text = monthArray[currentMonth]
        yearNumberLabel.text = "\(currentYear!)"
        
        if currentYear < thisYear {
            if thisMonth == currentMonth {

                nextMonthDisable = false
                nextBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
                nextBtn.isHidden = false
            } else {
                nextMonthDisable = false
                nextBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
                nextBtn.isHidden = false
            }

            previousBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            previousBtn.isHidden = false

        } else {
            if thisMonth == currentMonth {
                nextMonthDisable = true
                nextBtn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
                nextBtn.isHidden = true
            } else {
                nextMonthDisable = false
                nextBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
                nextBtn.isHidden = false
            }

            previousBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            previousBtn.isHidden = false
        }
        
    }
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .gregorian)
        guard let todayDate = formatter.date(from: today) else { return nil }
//        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = calendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupMonthLayout(start: Int, numberOfDay: Int) {
        var startDay: Int = 0
        var endDay: Int = 0
        var currentIndex: Int = 0
        
        // ====================================
        // Dev
        
        // Test with original
//        startDay = start
        if developerMode {
            startDay = startDayOfMonth
        } else {
            startDay = start
        }
        // Test with fix startDayOfMonth
        //        startDay = startDayOfMonth//start
        
        
        // ====================================
        
        endDay = startDay + numberOfDay - 1
        let indexForStart = startDay
        print("startDay: \(startDay), endDay: \(endDay), start: \(start)")
        
        var index = 1
        // Setup button.tag
        for item in dayBtns {
            if item.tag < startDay || item.tag > endDay {
                item.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                item.setTitle("", for: .normal)
                item.isHidden = true
            } else {
//                print("item.tag: \(item.tag), index: \(index)")
                currentIndex = index - indexForStart
                item.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                let numberToShow = currentIndex + 1
                item.setTitle("\(numberToShow)", for: .normal)
                item.isHidden = false
                // Hightlight on first load
//                print("CurrentDay: \(currentDay), currentIndex: \(currentIndex), numberToShow: \(numberToShow)")
                if currentDay == numberToShow {
                    item.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                    item.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.3647058824, blue: 0.8549019608, alpha: 1)
                    item.layer.borderWidth = 1.0
                    item.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    item.layer.cornerRadius = item.frame.size.width / 2
                    item.clipsToBounds = true
                } else {
                    item.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                }
                // Add Action
//                print("CurrentYear: \(currentYear), thisYear: \(thisYear)")
                if currentYear < thisYear {
                    item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                } else {
//                    print("CurrentMonth: \(currentMonth), thisMonth: \(thisMonth)")
                    if currentMonth == thisMonth {
                        // Month = Month
                        if currentDay - 1 < currentIndex {
                            item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
                        } else {
                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                        }
                    } else {
                        item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                    }
                }
//                print("Add action: ======> item.tag: \(item.tag), index: \(index)")
//                print("Remove action: ======> item.tag: \(item.tag), index: \(index)")
//                item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
            }
            index += 1
        }
    }
    
   
}
