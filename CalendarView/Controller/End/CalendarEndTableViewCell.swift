//
//  CalendarEndTableViewCell.swift
//  CalendarView
//
//  Created by Jakkawad Chaiplee on 5/9/2561 BE.
//  Copyright © 2561 Jakkawad Chaiplee. All rights reserved.
//

import UIKit

class CalendarEndTableViewCell: UITableViewCell {

    // Dev
    var startDayOfMonth: Int = 0
    var developerMode: Bool!
    //
    
    var startCalendarYear: Int!
    var startCalendarMonth: Int!
    var startCalendarDay: Int!
    
    var currentDay: Int!
    var currentMonth: Int!
    var currentYear: Int!
    
    var thisMonth: Int!
    var thisYear: Int!
    
    var startDate: String!
    
    var monthArray = ["Empty", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    
    
    
    var nextMonthDisable: Bool = true
    var previousDisable: Bool = true
    
    @IBOutlet weak var dayNumberLabel: UILabel!
    @IBOutlet weak var monthNameLabel: UILabel!
    @IBOutlet weak var yearNumberLabel: UILabel!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var previousBtn: UIButton!
    
    @IBOutlet var dayBtns: [UIButton] = []
    
    @IBAction func nextBtnWasPressed(_ sender: Any) {
        if nextMonthDisable {

        } else {
            nextMonth()
        }
    }
    
    @IBAction func previousBtnWasPressed(_ sender: Any) {
        if previousDisable {

        } else {
            previousMonth()
        }
    }
    func nextMonth() {
        if currentMonth > 11 {
            currentMonth = 1
            currentYear = currentYear + 1
        } else {
            currentMonth = currentMonth + 1
        }
        monthNameLabel.text = monthArray[currentMonth]
        
        setupCalendarUI()
        
        clearStartDateSelected()
    }
    
    func previousMonth() {
        if currentMonth <= 1 {
            currentMonth = 12
            currentYear = currentYear - 1
        } else {
            currentMonth = currentMonth - 1
        }
        monthNameLabel.text = monthArray[currentMonth]
        
        setupCalendarUI()
        
        clearStartDateSelected()
    }
    func clearStartDateSelected() {
        for item in dayBtns {
            item.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            item.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
        }
        startDate = ""
    }
    @objc func actionButtonWasPressed(sender:UIButton!) {
        for item in dayBtns {
            if item.tag == sender.tag {
                item.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                item.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.3647058824, blue: 0.8549019608, alpha: 1)
                item.layer.borderWidth = 1.0
                item.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                item.layer.cornerRadius = item.frame.size.width / 2
                item.clipsToBounds = true
                
                startDate = item.titleLabel?.text
                // NotiCenter => data
//                let passDate = (Int(startDate), currentMonth, currentYear)
//                notiCenter.post(name: .alertOverviewCellStartDate, object: passDate)
            } else {
                item.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                item.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            }
        }
    }
    func setDate(day: Int, month: Int, year: Int, sYear: Int, sMonth: Int, sDay: Int) {
        currentDay = day
        currentMonth = month
        currentYear = year
        thisMonth = currentMonth
        thisYear = currentYear
        startCalendarYear = sYear
        startCalendarMonth = sMonth
        startCalendarDay = sDay
        setupCalendarUI()
    }
    func setupCalendarUI() {
        // Get Date range
        let calendar = Calendar.current
        let dateComponents = DateComponents(year: currentYear, month: currentMonth)
        let didDate = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: didDate)!
        let numDays = range.count
        var startDayOfMonth: Int = 0
        
        for item in 0..<numDays {
            
            let day = item + 1
            let workDay = getDayOfWeek("\(currentYear!)-\(currentMonth!)-\(day)")
            if day == 1 {
                startDayOfMonth = workDay!
            }
            
        }
        if numDays == 28 {
            setupMonthLayout(start: startDayOfMonth, numberOfDay: 28)
        } else if numDays == 29 {
            setupMonthLayout(start: startDayOfMonth, numberOfDay: 29)
        } else if numDays == 30 {
            setupMonthLayout(start: startDayOfMonth, numberOfDay: 30)
        } else if numDays == 31 {
            setupMonthLayout(start: startDayOfMonth, numberOfDay: 31)
        }
        
        dayNumberLabel.text = "\(currentDay!)"
        monthNameLabel.text = monthArray[currentMonth]
        yearNumberLabel.text = "\(currentYear!)"
        
        // Next button
        nextMonthDisable = false
        nextBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
        nextBtn.isHidden = false
        // Previous button
        previousDisable = false
        previousBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
        previousBtn.isHidden = false
        if startCalendarYear == thisYear {
            if currentMonth == thisMonth {
                nextMonthDisable = true
                nextBtn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
                nextBtn.isHidden = true
            } else {
                nextMonthDisable = false
                nextBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
                nextBtn.isHidden = false
            }
            if startCalendarMonth == currentMonth {
                previousDisable = true
                previousBtn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
                previousBtn.isHidden = true
            } else {
                previousDisable = false
                previousBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
                previousBtn.isHidden = false
            }
        } else {
            if startCalendarYear == currentYear {
                if startCalendarMonth == currentMonth {
                    previousDisable = true
                    previousBtn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
                    previousBtn.isHidden = true
                } else {
                    previousDisable = false
                    previousBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
                    previousBtn.isHidden = false
                }
            } else {
                if currentYear == thisYear {
                    if currentMonth == thisMonth {
                        nextMonthDisable = true
                        nextBtn.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
                        nextBtn.isHidden = true
                    } else {
                        
                    }
                } else {
                    nextMonthDisable = false
                    nextBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
                    nextBtn.isHidden = false
                }
            }
        }
    }
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .gregorian)
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    func setupMonthLayout(start: Int, numberOfDay: Int) {
        var startDay: Int = 0
        var endDay: Int = 0
        var currentIndex: Int = 0
        
        
        // ====================================
        // Dev
        
        // Test with original
//        startDay = start
        if developerMode {
            startDay = startDayOfMonth
        } else {
            startDay = start
        }
        
        // Test with fix startDayOfMonth
//        startDay = startDayOfMonth//start
        
        
        // ====================================
        
        
        endDay = startDay + numberOfDay - 1
        let indexForStart = startDay
        print("startDay: \(startDay), endDay: \(endDay), start: \(start)")
        
        var index = 1
        for item in dayBtns {
            if item.tag < startDay || item.tag > endDay {
                item.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                item.setTitle("", for: .normal)
                item.isHidden = true
            } else {
//                print("item.tag: \(item.tag), index: \(index)")
                currentIndex = index - indexForStart
                item.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                let numberToShow = currentIndex + 1
                item.setTitle("\(numberToShow)", for: .normal)
                item.isHidden = false
                // Hightlight on first load
                if currentDay == numberToShow {
                    item.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                    item.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.3647058824, blue: 0.8549019608, alpha: 1)
                    item.layer.borderWidth = 1.0
                    item.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    item.layer.cornerRadius = item.frame.size.width / 2
                    item.clipsToBounds = true
                } else {
                    item.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                }
                print("========================================")
//                print("startCalendarYear: \(startCalendarYear), thisYear: \(thisYear), currentYear: \(currentYear)")
//                print("startCalendarDay: \(startCalendarDay), currentIndex: \(currentIndex), currentDay: \(currentDay)")
//                print("startCalendarMonth: \(startCalendarMonth), currentMonth: \(currentMonth), thisMonth: \(thisMonth)")
                print("startCalendarYear: \(startCalendarYear), currentYear: \(currentYear), thisYear: \(thisYear)")
                print("startCalendarMonth: \(startCalendarMonth), currentMonth: \(currentMonth), thisMonth: \(thisMonth)")
                print("startCalendarDay: \(startCalendarDay), currentDay: \(currentDay), numberOfShow: \(numberToShow)")
                // Action
                if startCalendarYear < thisYear {
//                    print("<<<DFDF<D<FD<<")
                    if currentYear == thisYear {
//                        print("Y===Y")
                        if currentMonth == thisMonth {
//                        print("========")
                            if currentDay >= numberToShow {
//                            print("REMOVE")
                                item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                            } else {
//                            print("ADD")
                                item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
                            }
                        } else {
//                            print("!!!!!!!!")
                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                        }
                    } else {
//                        print("Y!!!!Y")
                        if startCalendarMonth == currentMonth {
//                            print("U+U+U+U+U+U+U")
                            if startCalendarYear == currentYear {
//                                print("H(H(H(H((H(")
                                
                                if numberToShow < startCalendarDay {
//                                    print("ADD")
                                    item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
                                } else {
//                                    print("REMOVE")
                                    item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                                }
                            } else {
//                                print("T^T^T^T^T^T^T^")
                                item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                            }
                        } else {
//                            print("R%R%R%R%R%R%R%R")
                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                        }
                        
                    }
//                    if currentMonth == thisMonth {
////                        print("========")
//                        if currentDay >= numberToShow {
////                            print("REMOVE")
//                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                        } else {
////                            print("ADD")
//                            item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
//                        }
//                    } else {
//                        print("!!!!!!!!")
//                    }
                } else {
//                    print("ESLRELSRJSELRJE")
                    if startCalendarMonth == currentMonth {
//                        print("======")
                        if currentMonth == thisMonth {
//                            print("EE+E+E+E+E+E+E")
                            if numberToShow >= startCalendarDay {
//                            print("ADD")
                                if numberToShow <= currentDay {
//                                print("SHOW")
                                    item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                                } else {
//                                print("HIDE")
                                    item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
                                }
                            } else {
//                            print("REMOVE")
                                item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
                            }
                        } else {
//                            print("!E!E!E!E!E!E!E!")
                            if numberToShow >= startCalendarDay {
//                                print("ADD")
                                item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                            } else {
//                                print("REMOVE")
                                item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
                            }
                        }
                        
                    } else {
//                        print("!!!!!!")
                        if numberToShow > currentDay {
//                            print("REMOVE")
                            item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
                        } else {
//                            print("ADD")
                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
                        }
                    }
                }
//                if startCalendarYear == thisYear {
////                    print("===")
//                    if startCalendarDay <= currentIndex {
//                        if currentIndex > currentDay {
//                            item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
//                        } else {
//                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                        }
//                    } else {
//                        item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
//                    }
//                } else {
//                    if startCalendarYear == currentYear {
////                        print("CAT")
//                        if startCalendarMonth == currentMonth {
//                            if startCalendarDay <= currentIndex {
//                                item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                            } else {
//                                item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
//                            }
//                        } else {
//                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                        }
//                    } else {
//                        if thisMonth == currentMonth {
//                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                        } else {
//                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                        }
//                    }
//                }
//                if startCalendarYear < thisYear {
//                    print("CAT")
//                } else {
//                    print("FOX")
//                    if thisMonth == currentMonth {
//                        print("====")
//                    } else {
//                        print("!!!!")
//                    }
//                }
//                if startCalendarYear < thisYear {
//                    print("<<<<<<")
//                } else {
//                    print("!!!!!!")
//                }
//                if startCalendarYear == thisYear {
//                    print("YEAR ==")
//                    if startCalendarMonth == currentMonth {
//                        print("MONTH ===== C")
//                    } else {
//                        print("MONTH !!!!!! C")
//                    }
//                    if startCalendarMonth == currentMonth {
//                        print("MONTH ======")
//                        if startCalendarYear == currentYear {
//                            print("YEARRRR =====")
//                        } else {
//                            print("YEARRRRR !!!!!!")
//                        }
//                        // startCalendarDay
//                        // numberToShow > currentDay -> remove
//                        // 6 -> 6 -> add
//                        // 7 -> 6 -> remove
//                        // 5 -> 6 -> add
//                    } else if startDayOfMonth < currentMonth {
//                        print("MONTHTT <<<<<<<")
//                    } else {
//                        print("MONTH !!!!!!!")
//                        // add
//                    }
//                } else {
//                    print("YEAR !!")
//                    if startCalendarYear == currentYear {
//                        print("CAT")
//                    } else {
//                        print("HOG")
//                        if currentMonth == startCalendarMonth {
//                            print("PIG")
//                            if startCalendarYear == currentYear {
//                                print("FOX")
//                            } else {
//                                print("COW")
//                            }
//                        } else {
//                            print("DOG")
//                        }
//                    }
//                    if startCalendarMonth < currentMonth {
//                        print("MONTH <<<")
//                    } else {
//                        print("MONTH >>>")
//                        // day < startDay
//                    }
//                    if startCalendarMonth == currentMonth {
//                        print("MONTH ===")
//                    } else {
//                        print("MONTH !!!")
                        // day < startDay
//                    }
//                    if startCalendarMonth == currentMonth {
//                        print("CAT =====")
//                    } else {
//                        print("CAT !!!!!!")
//                    }
//                }
//                if currentYear < thisYear {
//
//                } else {
//                    if currentMonth == thisMonth {
//                        // Month = Month
//                        if currentDay - 1 < currentIndex {
//                            item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
//                        } else {
//                            item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                        }
//                    } else {
//                        item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                    }
//                }
//                print("Add action: ======> item.tag: \(item.tag), index: \(index)")
//                print("Remove action: ======> item.tag: \(item.tag), index: \(index)")
//                item.addTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .touchUpInside)
//                item.removeTarget(self, action: #selector(actionButtonWasPressed(sender:)), for: .allEvents)
            }
            index += 1
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
