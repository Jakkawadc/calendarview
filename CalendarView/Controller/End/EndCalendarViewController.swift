//
//  EndCalendarViewController.swift
//  CalendarView
//
//  Created by Jakkawad Chaiplee on 5/9/2561 BE.
//  Copyright © 2561 Jakkawad Chaiplee. All rights reserved.
//

import UIKit

class EndCalendarViewController: UIViewController {

    var startDayOfMonth: Int = 0
    
    var developerMode: Bool = false
    
    var startDay: Int = 0
    var startMonth: Int = 0
    var startYear: Int = 0
    
    var currentDay: Int = 0
    var currentMonth: Int = 0
    var currentYear: Int = 0
    
    let dateCell = "dateCell"
    let dateCellHeight: CGFloat = 480.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        print("Start day: \(startDay),  month: \(startMonth), year: \(startYear), startDayOfMonth: \(startDayOfMonth)")
//        print("Current day: \(currentDay), month: \(currentMonth), year: \(currentYear)")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EndCalendarViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dateCellHeight
    }
}

extension EndCalendarViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: dateCell) as? CalendarEndTableViewCell
        cell?.developerMode = developerMode
        cell?.startDayOfMonth = startDayOfMonth
        cell?.setDate(day: currentDay, month: currentMonth, year: currentYear, sYear: startYear, sMonth: startMonth, sDay: startDay)
        cell?.selectionStyle = .none
        return cell!
    }
}
