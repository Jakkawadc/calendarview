//
//  ViewController.swift
//  CalendarView
//
//  Created by Jakkawad Chaiplee on 14/8/2561 BE.
//  Copyright © 2561 Jakkawad Chaiplee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var startDayOfMonth: Int = 1
    var startDay: Int = 1
    var startMonth: Int = 1
    var startYear: Int = 2018
    
    var endDayOfMonth: Int = 1
    var endDay: Int = 1
    var endMonth: Int = 1
    var endYear: Int = 2018
    
    var developerMode: Bool = true
    
    
    @IBOutlet weak var startDayOfMonthLabel: UILabel!
    @IBOutlet weak var startDayLabel: UILabel!
    @IBOutlet weak var startMonthLabel: UILabel!
    @IBOutlet weak var startYearLabel: UILabel!
    @IBOutlet weak var startDayOfMonthStepper: UIStepper!
    @IBOutlet weak var startDayStepper: UIStepper!
    @IBOutlet weak var startMonthStepper: UIStepper!
    @IBOutlet weak var startYearStepper: UIStepper!
    
    @IBOutlet weak var endDayOfMonthLabel: UILabel!
    @IBOutlet weak var endDayLabel: UILabel!
    @IBOutlet weak var endMonthLabel: UILabel!
    @IBOutlet weak var endYearLabel: UILabel!
    @IBOutlet weak var endDayOfMonthStepper: UIStepper!
    @IBOutlet weak var endDayStepper: UIStepper!
    @IBOutlet weak var endMonthStepper: UIStepper!
    @IBOutlet weak var endYearStepper: UIStepper!
    
    @IBOutlet weak var developerModeLabel: UILabel!
    
    @IBOutlet weak var developerModeSwitch: UISwitch!
    
    @IBAction func developerModeSwtichWasPressed(_ sender: UISwitch) {
        sender.isOn = !sender.isOn
        developerMode = sender.isOn
        if sender.isOn {
            developerModeLabel.text = "Developer Mode: ON"
        } else {
            developerModeLabel.text = "Developer Mode: OFF"
        }
    }    
    
    @IBAction func startDayOfMonthStepperWasPressed(_ sender: UIStepper) {
        startDayOfMonthLabel.text = Int(sender.value).description
        startDayOfMonth = Int(sender.value)
    }
    
    @IBAction func startDayStepperWasPressed(_ sender: UIStepper) {
        startDayLabel.text = Int(sender.value).description
        startDay = Int(sender.value)
    }
    @IBAction func startMonthStepperWasPressed(_ sender: UIStepper) {
        startMonthLabel.text = Int(sender.value).description
        startMonth = Int(sender.value)
    }
    @IBAction func startYearStepperWasPressed(_ sender: UIStepper) {
        startYearLabel.text = Int(sender.value).description
        startYear = Int(sender.value)
    }
    
    @IBAction func endDayOfMonthStepperWasPressed(_ sender: UIStepper) {
        endDayOfMonthLabel.text = Int(sender.value).description
        endDayOfMonth = Int(sender.value)
    }
    
    @IBAction func endDayStepperWasPressed(_ sender: UIStepper) {
        endDayLabel.text = Int(sender.value).description
        endDay = Int(sender.value)
    }
    @IBAction func endMonthStepperWasPressed(_ sender: UIStepper) {
        endMonthLabel.text = Int(sender.value).description
        endMonth = Int(sender.value)
    }
    @IBAction func endYearStepperWasPressed(_ sender: UIStepper) {
        endYearLabel.text = Int(sender.value).description
        endYear = Int(sender.value)
    }
    
    func setupStartLabel() {
        startDayOfMonthStepper.value = Double(startDayOfMonth)
        startDayStepper.value = Double(startDay)
        startMonthStepper.value = Double(startMonth)
        startYearStepper.value = Double(startYear)
        
        startDayOfMonthLabel.text = "\(Int(startDayOfMonth))"
        
        startDayLabel.text = "\(Int(startDay))"
        startMonthLabel.text = "\(Int(startMonth))"
        startYearLabel.text = "\(Int(startYear))"
    }
    
    func setupEndLabel() {
        endDayOfMonthStepper.value = Double(endDayOfMonth)
        endDayStepper.value = Double(endDay)
        endMonthStepper.value = Double(endMonth)
        endYearStepper.value = Double(endYear)
        
        endDayOfMonthLabel.text = "\(Int(endDayOfMonth))"
        
        endDayLabel.text = "\(Int(endDay))"
        endMonthLabel.text = "\(Int(endMonth))"
        endYearLabel.text = "\(Int(endYear))"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupStartLabel()
        setupEndLabel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
     // MARK: - Navigation
    
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == "startSegue" {
            if let startVC = segue.destination as? StartCalendarViewController {
                startVC.startDay = startDay
                startVC.startMonth = startMonth
                startVC.startYear = startYear
                startVC.startDayOfMonth = startDayOfMonth
                
                startVC.developerMode = developerMode
            }
        } else if segue.identifier == "endSegue" {
            if let endVC = segue.destination as? EndCalendarViewController {
                endVC.startDay = startDay
                endVC.startMonth = startMonth
                endVC.startYear = startYear
                //
                endVC.currentDay = endDay
                endVC.currentMonth = endMonth
                endVC.currentYear = endYear
                endVC.startDayOfMonth = endDayOfMonth
                
                endVC.developerMode = developerMode
            }
        }
     }
    
}

